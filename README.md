# pequeno tutorial sobre crawlers

# 01 - urllib.request.urlopen

### O caso das API rest

Uma API facilita tudo, nem sempre para ser sincero mas na imensa maioria das vezes, com ela não precisamos fazer buscas em htmls imensos, há uma documentação na maioria das vezes que indica onde está o que queremos e temos um json bem arrumado quase sempre onde podemos acessar de forma mais direta as coisas, é assim que é no twitter por exemplo.

No momento não entrarei no mérito do json-rpc, nem de passagem de tokens via headers ou via post, o que é preciso no momento é apenas entender como python pode lidar com o json recebido por uma api, para isso vou usar uma api livre que serve apenas para testar a interação com um api: https://jsonplaceholder.typicode.com/

**json ~> javascript object notation**

json de javascript e dicionários de python implementam a mesma estrutura de dados: tabelas hash, que tem complexidade O1, ou seja, a velocidade de acesso aos dados é constante não importa o tamanho da tabela hash isso porque, ao funcionar no modelo chave-valor, a linguagem de programação internamente calcula um valor hexadecimal que representa um espaço na memória e assim ao inserir uma chave, o espaço na memória é acessado, tanto em python como em javascrip a sintaxe disso é exatamente a mesma.

```python
python_dict = {
	"key":"value"
}

value = python_dict["key"]
```

```javascript
json = {
	"key":"value"
}

value = json["key"]
ou
value = json.key
```

Python tem o módulo built-in `json` que como o nome mesmo indica, converte um json para um dicionário, nem sempre vai converter valores que seriam óbvios para a gente que são float, por exemplo, isso porque vai tentar preservar o formato original dos dados enviados pela api rest.

Então se fazemos uma busca, como de tweets de um usuário ou uma lista de usuários que seguimos, temos normalmente uma lista de jsons que é convertida numa lista de dicionários em nosso código:

```python
for users in list_of_users:
    print(users["username"], " - ", users["company"]["name"])
```

Raramente vamos usar urllib e json dessa forma, na quase totalidade dos casos comuns de uso há bibliotecas prontas que na prática vão realizar o mesmo processo: acessar a url com os dados de acesso (no caso do twitter são 4 chaves: 2 de aplicativo e 2 de usuário) e converter o resultado num dicionário que posteriormente é integrado a um objeto feito para facilitar a nevagação entre os dados, internamente cada classe criada em python é na verdade uma camada de abstração sobre dicionários, há o atributo `__dict__` em todos os objetos que criamos a partir das classes que fazemos exatamente por este motivo, mas esse é um assunto que vamos abordar melhor quando vermos mais de perto POO em python.
