from urllib.request import urlopen

url = "https://jsonplaceholder.typicode.com/users"

response = urlopen(url)

print(response.getheaders())

api_output = response.read() # pode ser como byte

import json

all_users = json.loads(api_output)

for users in all_users:
    print(users["username"], " - ", users["address"]["city"])
